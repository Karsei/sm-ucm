[한글]

# 유저 클래스 관리 #

이 프로젝트는 native 함수들을 사용하여 다른 플러그인으로 유저들의 등급을 효과적으로 조정할 수 있습니다.

## 플러그인 특징 ##

여러 가지 플러그인을 다루면서 유저들의 등급 정보에 대해 여러 가지를 위해서 이 플러그인을 사용하실 수 있습니다.

* 플러그인마다 유저들에 대한 클래스 담당 처리를 하지 않아도 됩니다.
* 웹 사이트나 데이터베이스를 통해 유저들의 등급을 효율적으로 관리할 수 있습니다.

## 개발 예정 사항 ##

* **[예정]** '언어 추가' 및 'ENV 추가' 버튼을 누를 시 이상하게 나타나는 점 수정
* **[예정]** 개발 문서 작성
* **[예정]** 웹 패널 인스톨 페이지 추가
## ##
[ENG]

# User Class Management #
This project can control user's class with another plugins using native functions efficiently.

## Plugin Features ##

You can use this with managing several plugins about users' class information for anything. This can control easier with class relevant systems.

* You don't make users' class controller in your plugins. Just use with This!
* You can use a simple way with using website or databases for managing users' class efficiently.